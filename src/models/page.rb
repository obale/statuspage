require_relative "../status_checker"

class Page
    include DataMapper::Resource

    property :id          , Serial
    property :name        , String
    property :description , Text
    property :slug        , String

    property :protocol    , String  , default: 'http'
    property :url         , String
    property :path        , String  , default: '/'
    property :port        , Integer , default: 80

    property :username    , String
    property :password    , String

    validates_uniqueness_of :slug

    def status
        @status ||= StatusChecker.new self.protocol, self.url, self.path

        unless self.username.nil? or self.password.nil?
            @status.http_auth username, password
        end

        @status
    end
end
